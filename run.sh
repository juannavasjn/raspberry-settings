sudo apt update
sudo apt-get install network-manager
curl -sL https://deb.nodesource.com/setup_10.x | bash -
sudo apt-get install -y nodejs
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
rm -f /etc/localtime
ln -sf /usr/share/zoneinfo/America/Caracas /etc/localtime

